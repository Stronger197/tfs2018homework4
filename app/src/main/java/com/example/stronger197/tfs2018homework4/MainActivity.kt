package com.example.stronger197.tfs2018homework4

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.example.stronger197.tfs2018homework4.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    // private var fragmentNumber = 0;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onBackPressed() {
        Log.d("BackPressed", supportFragmentManager.backStackEntryCount.toString())
        if (supportFragmentManager.backStackEntryCount - 1 != 0) {
            super.onBackPressed()
        } else {
            super.onBackPressed()
            emptyTextView.visibility = View.VISIBLE
        }
    }

    override fun onStart() {
        super.onStart()
        addButton.setOnClickListener {
            val counter = supportFragmentManager.backStackEntryCount + 1
            if (counter != 0) {
                emptyTextView.visibility = View.GONE
            }
            val fragment = DocumentFragment()
            val args = Bundle()
            args.putInt("FragmentNumber", counter)
            fragment.arguments = args

            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragmentContainer, fragment)
                    .addToBackStack(null)
                    .commit()

        }

        removeButton.setOnClickListener {
            val counter = supportFragmentManager.backStackEntryCount - 1
            if (counter == 0) {
                emptyTextView.visibility = View.VISIBLE
            }
            supportFragmentManager.popBackStack()
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putInt("TextViewState", emptyTextView.visibility)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        val textVisibility = savedInstanceState?.getInt("TextViewState")
        if (textVisibility != null) {
            emptyTextView.visibility = textVisibility
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        removeButton
    }
}
